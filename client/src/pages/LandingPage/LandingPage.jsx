import { Link } from "react-router-dom";
import styles from "./LandingPage.module.css";

const LandingPage = () => {
  return (
    <main className={styles.container}>
      <h1>Landing Page</h1>
      <Link to="home" className="button button-primary">
        Ingresar
      </Link>
    </main>
  );
};

export default LandingPage;

import { useSelector } from "react-redux";
import NavBar from "../../components/layouts/NavBar/NavBar";

const HomePage = () => {
  const { recipes } = useSelector((state) => state.recipes);
  const { value } = useSelector((state) => state.contador);
  console.log(recipes);
  return (
    <div>
      <NavBar />
      <h1>Home Page</h1>
      {value}
    </div>
  );
};

export default HomePage;

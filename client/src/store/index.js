import {configureStore} from "@reduxjs/toolkit"
import recipes from "./slices/recipes"
import contador from "./slices/contador"

export default configureStore({
    reducer: {recipes, contador}

})

import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  recipes: [],
};

export const recipesSlice = createSlice({
  name: "recipes",
  initialState,
  reducers: {
    setRecipes: (state, action) => {
      state.recipes = action.payload;
    },
  },
});

export const { setRecipes } = recipesSlice.actions;
export const fetchAllRecipes = () => async (dispatch) => {
  console.log("axios");
  const response = await axios.get("http://localhost:3001/api/v2/food/recetas");
  console.log(response);
  dispatch(setRecipes(response.data.recetas));
};
export default recipesSlice.reducer;

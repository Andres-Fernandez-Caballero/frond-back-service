import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    value: 0
}

export const contadorSlice = createSlice({
    name: "contador",
    initialState,
    reducers: {
        incrementar: (state,action) => {
            state.value = state.value + 1
        },
        decrementar: (state,action) =>{
            state.value = state.value -1
        },
        incrementarPorCantidad: (state,action) =>{
            state.value  = state.value + action.payload
        },
        reiniciar: (state,action) =>{
            state.value = 0
        }

    }
})

export const {incrementar,decrementar,incrementarPorCantidad,reiniciar} = contadorSlice.actions

export default contadorSlice.reducer
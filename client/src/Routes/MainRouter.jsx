import { Route, Routes } from "react-router-dom";
import CreateRecipy from "../pages/CreateRecipy";
import DetailRecipy from "../pages/DetailRecipy";
import HomePage from "../pages/HomePage/HomePage";
import LandingPage from "../pages/LandingPage";
import NotFound from "../pages/NotFound";

export default function MainRouter() {
  return (
    <Routes>
      <Route path="/" element={<LandingPage />} />
      <Route path="/home" element={<HomePage />} />
      <Route path="/create" element={<CreateRecipy />} />
      <Route path="/detail/:id" element={<DetailRecipy />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

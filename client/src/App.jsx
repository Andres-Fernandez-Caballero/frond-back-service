import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import MainRouter from "./Routes/MainRouter";
import { fetchAllRecipes } from "./store/slices/recipes";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    console.log("conectando a la API");
    dispatch(fetchAllRecipes());
  }, []);

  return (
    <div className="App">
      <BrowserRouter>
        <MainRouter />
      </BrowserRouter>
    </div>
  );
}

export default App;

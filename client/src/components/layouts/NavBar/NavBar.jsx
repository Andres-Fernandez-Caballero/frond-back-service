import { NavLink } from "react-router-dom";
import styles from "./NavBar.module.css";
import logo from "./../../../res/recipe-book.png";

const NavBar = () => {
  const links = [
    { to: "/home", text: "Home" },
    { to: "/create", text: "Create" },
  ];
  //
  const activeNavStyle = { color: "whitesmoke" };
  return (
    <nav className={styles.navbar}>
      <img src={logo} className={styles.navbar_logo} alt="logo" width={80} />
      <h2 className={styles.navbar__title}>Brand</h2>
      <ul className={styles.navbar__links}>
        {links.map((link) => (
          <li key={link.to}>
            <NavLink
              to={link.to}
              style={({ isActive }) => (isActive ? activeNavStyle : {})}
            >
              {link.text}
            </NavLink>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default NavBar;

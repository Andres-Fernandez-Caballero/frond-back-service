const { DESALLUNO, CENA, SNACK, ALMUERZO } = require("./constants/dishesTypes");
const db = require("./db");

async function cargarDatosDePrueba() {

    db.DietType.create({
        name: "Bien ARGENTO",
    });

    db.DietType.create({
        name: "Vegano",
    });

    db.DietType.create({
        name: "Vegetariano",
    });

    db.DietType.create({
        name: "Sin gluten",
    });

    db.Recipe.create({
        title: "Tortilla de papas",
        summary: "Una tortilla de papas",
        healthScore: 100,
        dishTypes: [DESALLUNO, CENA, SNACK, ALMUERZO],


    });

    db.Recipe.create({
        title: "milanesa napolitana",
        summary: "Una milanesa napolitana",
        healthScore: 70,
        dishTypes: [CENA, ALMUERZO],
    });
}

module.exports = cargarDatosDePrueba;
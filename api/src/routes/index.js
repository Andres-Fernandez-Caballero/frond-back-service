const { Router } = require('express');
const apiV2Router = require('./api/v2');
// Importar todos los routers;
// Ejemplo: const authRouter = require('./auth.js');


const router = Router();

router.use("/api/v2", apiV2Router)
router.get("/", (req,res) => {res.send ("Backend del proyecto")})
router.get("*", (req,res) => {res.send("404 not found")})
// Configurar los routers
// Ejemplo: router.use('/auth', authRouter);


module.exports = router;

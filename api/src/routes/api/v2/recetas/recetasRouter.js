const express = require ("express")
const recetasController = require("../../../../controllers/recetasController")
const recetasRouter = express.Router ()

recetasRouter.get("/", recetasController.list)
recetasRouter.get("/:id", recetasController.detail)
recetasRouter.post("/", recetasController.store)
recetasRouter.put("/:id", recetasController.update)
recetasRouter.delete("/:id", recetasController.remove)

module.exports = recetasRouter 
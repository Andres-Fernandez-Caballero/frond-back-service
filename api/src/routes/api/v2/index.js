const express = require ("express")
const recetasRouter = require("./recetas/recetasRouter")
const apiV2Router = express.Router()

apiV2Router.use("/food/recetas", recetasRouter)

module.exports = apiV2Router

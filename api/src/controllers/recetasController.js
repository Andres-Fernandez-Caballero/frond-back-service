const recetasService = require("../services/recetasService");

const recetasController = {
  list: async (req, res) => {
    try {
      const respuesta = await recetasService.list();
      res
        .status(200)
        .json({
          status: 200,
          mensaje: "ok",
          recetas: respuesta,
          cantidad: respuesta.length,
        });
    } catch (error) {
      res.status(400).json({ status: 400, mensaje: "error de base de datos DETALLE: " + error });
    }
  },

  detail: async (req, res) => {
    try {
      const respuestaDetail = await recetasService.detail(req.params.id);
      res
        .status(200)
        .json({ status: 200, mensaje: "ok", detalle: respuestaDetail });
    } catch (error) {
      res
        .status(400)
        .json({ status: 400, mensaje: "error de detalles" + error });
    }
  },

  store: async (req, res) => {
    const { name } = req.body;
    //validacion de formulario
    const receta = { name };
    const respuestaStore = await recetasService.store(receta);
    res.send(respuestaStore);
  },

  update: (req, res) => {
    const respuestaUpdate = recetasService.update(req.params.id);
    res.send(respuestaUpdate);
  },

  remove: (req, res) => {
    const respuestaRemove = recetasService.remove(req.params.id);
    res.send(respuestaRemove);
  },
};

module.exports = recetasController;

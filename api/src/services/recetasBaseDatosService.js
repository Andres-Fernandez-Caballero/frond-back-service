const db = require("../db");

const recetasBaseDatosSercice = {
    list: async () => {
        const recetasDesdeBaseDeDatos = await db.Recipe.findAll();
        return recetasDesdeBaseDeDatos;
    }
}

module.exports = recetasBaseDatosSercice;
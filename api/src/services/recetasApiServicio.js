const axios = require("axios");

const recetasApiServicio = {
    list: async () => {
        const urlRecetasList =
            process.env.API_FOOD_URL +
            "/recipes/complexSearch?apiKey=" +
            process.env.API_FOOD_KEY +
            "&addRecipeInformation=true&number=" +
            process.env.API_FOOD_NUMBER_RECIPES;

        const respuesta = await axios.get(urlRecetasList);
        const recetasApiMapeadas = respuesta.data.results.map((receta) => {
            return {
                id: receta.id,
                title: receta.title,
                summary: receta.summary,
                healthScore: receta.healthScore,
                dishTypes: receta.dishTypes,
                image: receta.image,
                diets: receta.diets,
            };
        });
        return recetasApiMapeadas;
    }
}

module.exports = recetasApiServicio;
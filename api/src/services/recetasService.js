const sourceIsDataBase = require("../../tools/sourceSelector");
const recetasApiServicio = require("./recetasApiServicio");
const recetasBaseDatosSercice = require("./recetasBaseDatosService");


const recetasService = {
  list: async () => {
    const recetasApiMapeadas = await recetasApiServicio.list();
    const recetasBaseDeDatos = await recetasBaseDatosSercice.list();

    const todasLasRecetas = [...recetasBaseDeDatos, ...recetasApiMapeadas];
    return todasLasRecetas;
  },

  detail: async (id) => {
    if (sourceIsDataBase(id)) {
      console.log("entro a la base de datos: " + id);
      const detalle = await db.Recipe.findByPk(id);
      if (detalle === null)
        // {throw Error("La receta no existe")}
        return detalle;
    } else {
      console.log("entro a la api");
      return "Buscando en Api";
    }
  },

  store: async (receta) => {
    const nuevaReceta = await db.Recipe.create(receta);
    return nuevaReceta;
  },

  update: (id) => {
    return "Receta con id" + id + "editada";
  },

  remove: (id) => {
    return "Receta con id" + id + "eliminada";
  },
};

module.exports = recetasService;

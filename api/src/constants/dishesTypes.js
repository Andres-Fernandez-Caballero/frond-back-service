const dishesTypes = {
    DESALLUNO: 'desayuno',
    ALMUERZO: 'almuerzo',
    CENA: 'cena',
    SNACK: 'snack',
}

module.exports = dishesTypes;